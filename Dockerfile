FROM openjdk:8-alpine
ADD build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]

